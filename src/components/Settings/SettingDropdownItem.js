import React from 'react';

export default function SettingDropdownItem({ label, data, inputName }) {
    return (
        <div className="setting-dropdown-wrap">
            <div className="setting-meta">
                <div className="setting-label">{label}</div>
            </div>

            <div className="setting-action">
                <div className="setting-dropdown-input">
                    <select name={inputName} className="setting-dropdown">
                        {data.map((item, index) => (
                            <option key={index} value={item.value}>
                                {item.label}
                            </option>
                        ))}
                    </select>
                    <div className="dropdown-icon">
                        <img
                            src="/images/arrow-dropdown.png"
                            alt="arrow-dropdown"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
