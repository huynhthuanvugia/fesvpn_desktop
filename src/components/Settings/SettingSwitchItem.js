import React from 'react';
import { Form } from 'react-bootstrap';

export default function SettingSwitchItem({ label, desc }) {
    return (
        <div className={`setting-switcher ${!desc && 'setting-switcher-nodesc'}`}>
            <div className="setting-meta">
                <div className="setting-label">{label}</div>
                <div className="setting-desc">{desc}</div>
            </div>

            <div className="setting-action">
                <Form.Check type="switch" className="custom-switcher" />
            </div>
        </div>
    );
}
