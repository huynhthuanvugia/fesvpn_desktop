import React from 'react';
import SettingApplication from '../SettingApplication';
import SettingDropdownItem from '../SettingDropdownItem';
import SettingSwitchItem from '../SettingSwitchItem';

const dataLocationDropdown = [
    {
        value: 'en',
        label: 'England',
    },
    {
        value: 'vi',
        label: 'Vietnam',
    },
    {
        value: 'cn',
        label: 'China',
    },
    {
        value: 'us',
        label: 'United Kingdom',
    },
];

export default function TabGeneral() {
    return (
        <div className="tab-setting-wrap tab-general">
            <SettingSwitchItem
                desc={'Automatically start the app when the system starts'}
                label={'Auto start with Window'}
            />
            <SettingSwitchItem
                desc={'Automatically start the app when the system starts'}
                label={'Connect on start'}
            />
            <SettingDropdownItem
                inputName={'setting-dropdown'}
                label={'Select server location'}
                data={dataLocationDropdown}
            />
            <SettingSwitchItem label={'Allow notification'} />
            <SettingSwitchItem
                desc={'Disable your internet connection when VPN disconnects'}
                label={'Internet Kill switch'}
            />
            <SettingSwitchItem
                desc={'Choose which applications use the VPN'}
                label={'App Spit tunnel'}
            />
        </div>
    );
}
