import React from 'react';
import SettingDropdownItem from '../SettingDropdownItem';
import SettingSwitchItem from '../SettingSwitchItem';

const dataProtocol = [
    {
        value: 'en',
        label: 'England',
    },
    {
        value: 'vi',
        label: 'Vietnam',
    },
    {
        value: 'cn',
        label: 'China',
    },
    {
        value: 'us',
        label: 'United Kingdom',
    },
];

export default function TabProtocol() {
    return (
        <div className="tab-setting-wrap tab-protocol">
            <SettingSwitchItem
                desc={'Automatically start the app when the system starts'}
                label={'Automatic'}
            />
            <SettingDropdownItem
                inputName={'setting-dropdown'}
                label={'VPN protocol'}
                data={dataProtocol}
            />
        </div>
    );
}
