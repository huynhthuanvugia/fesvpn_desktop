import React from 'react';
import { Button } from 'react-bootstrap';

export default function SettingApplication({ label }) {
    return (
        <div className="setting-application">
            <div className="setting-action">
                <Button className="btn-add-application">+</Button>
            </div>
            <div className="setting-meta">
                <div className="setting-label">{label}</div>
            </div>
        </div>
    );
}
