import React from 'react';
import { Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';

export default function FormLogin() {
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    const onSubmit = (data) => console.log(data);

    return (
        <div className="form-login-wrap">
            <div className="form-logo">
                <img src="/images/logo-larger.png" alt="logo-larger" />
            </div>
            <form
                className="form-login-inner"
                onSubmit={handleSubmit(onSubmit)}
            >
                <input
                    type="text"
                    className="form-control"
                    placeholder="Email"
                    {...register('email', { required: true })}
                />
                {errors.password && (
                    <span className="form-error">This field is required</span>
                )}

                <input
                    type="password"
                    placeholder="Password"
                    className="form-control"
                    {...register('password', { required: true })}
                />
                {errors.password && (
                    <span className="form-error">This field is required</span>
                )}
                <Link to={'/forgot-password'} className="link-form">
                    Forgot Password ?
                </Link>
                <div className="form-action">
                    <Button className="btn-login">Login</Button>

                    <div className="action-login">
                        <div className="action-text">
                            Don’t have an account?{' '}
                            <Link to={'/sign-up'} className="action-link">
                                Start Free Trial
                            </Link>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}
