import React from 'react';

export default function ButtonConnect() {
    React.useEffect(() => {
        let btnConnect = document.querySelector('.btn-start-new');

        if (btnConnect) {
            btnConnect.onclick = function () {
                btnConnect.classList.add('load');
                setTimeout(function () {
                    btnConnect.classList.add('done');
                }, 1000);
                setTimeout(function () {
                    btnConnect.classList.add('active');
                }, 2000);
                // setTimeout(function () {
                //     btnConnect.classList.remove('load', 'done');
                // }, 5000);
            };
        }
    }, []);

    return (
        <div className="connect-start">
            <div className="btn-start-new">
                <svg
                    width="112"
                    height="112"
                    viewBox="0 0 112 112"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <circle
                        cx="56"
                        cy="56"
                        r="55"
                        fill="white"
                        stroke="red"
                        strokeWidth="2"
                        style={{
                            position: 'absolute',
                            transform: 'rotate(-90deg)',
                            transformOrigin: '50% 50%',
                        }}
                    />
                    <ellipse
                        cx="55.7176"
                        cy="56"
                        rx="47.7176"
                        ry="48"
                        fill="#6083FF"
                    />
                    <g clipPath="url(#clip0_892_3)">
                        <path
                            d="M55.8838 39.7488C55.2813 39.8971 54.7187 40.1187 54.2834 40.5841C53.7712 41.1331 53.5535 41.7938 53.5521 42.5278C53.5491 46.8292 53.5461 51.132 53.5535 55.4334C53.5565 56.7972 54.2923 57.7675 55.5566 58.1287C57.0608 58.5588 58.6997 57.4475 58.8418 55.8973C58.8685 55.611 58.8833 55.3218 58.8833 55.0355C58.8862 50.9103 58.8833 46.7866 58.8862 42.6614C58.8862 41.8936 58.7101 41.1948 58.1756 40.6091C57.7374 40.1275 57.1674 39.8985 56.5545 39.7473C56.3309 39.7488 56.1074 39.7488 55.8838 39.7488Z"
                            fill="white"
                        />
                        <path
                            d="M43.1591 56.3496C43.2538 52.8234 44.8261 49.4689 47.9781 46.8646C48.8886 46.1115 49.1462 45.1381 48.6606 44.2485C48.055 43.1372 46.6323 42.8509 45.6167 43.6672C42.1553 46.452 40.0205 50.0135 39.3705 54.3986C38.1403 62.6989 43.091 70.5191 51.1433 73.05C58.0661 75.2257 65.7912 72.6728 70.0269 66.8197C71.796 64.3724 72.9049 61.661 73.1699 58.6632C73.6822 52.8791 71.7516 48.0155 67.3768 44.134C67.0244 43.8213 66.6262 43.5218 66.1969 43.3383C65.3426 42.9742 64.3611 43.3398 63.8607 44.1223C63.3573 44.9091 63.4609 45.9147 64.1271 46.5621C64.7963 47.214 65.5336 47.8056 66.1406 48.5088C68.5286 51.2746 69.599 54.516 69.2008 58.1201C68.6456 63.1437 65.9496 66.736 61.3068 68.8147C59.0047 69.8453 56.553 70.1242 54.0569 69.6633C49.4186 68.8074 46.1467 66.1943 44.1984 61.9516C43.5174 60.4704 43.1369 58.4577 43.1591 56.3496Z"
                            fill="white"
                        />
                    </g>
                    <defs>
                        <clipPath id="clip0_892_3">
                            <rect
                                width="34.0704"
                                height="34.0704"
                                fill="white"
                                transform="matrix(-1 0 0 1 73.2512 39.7488)"
                            />
                        </clipPath>
                    </defs>
                </svg>
            </div>
        </div>
    );
}
