import React from 'react';
import { Button } from 'react-bootstrap';

export default function Remain() {
    return (
        <div className="app-remain">
            <div className="remain-value">4.7 GB remaining</div>
            <Button className="remain-btn">
                <img
                    src="/images/icon-upgrade.png"
                    alt="icon-upgrade"
                    className="remain-icon"
                />
                <span className="remain-btn-text">Upgrade Now</span>
            </Button>
        </div>
    );
}
