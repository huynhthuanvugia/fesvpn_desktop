import React from 'react';

export default function TimeRemain() {
    return (
        <div className="time-remain">
            <div className="time-remain-label">Protected</div>
            <div className="time-remain-countdown">05:00:10</div>
        </div>
    );
}
