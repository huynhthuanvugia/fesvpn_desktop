import React from 'react';

export default function LocationBackground() {
    return (
        <div className="location-background">
            <img src="/images/bg-en.png" alt="location-en" />
        </div>
    );
}
