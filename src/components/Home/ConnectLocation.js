import React from 'react';
import { Link } from 'react-router-dom';

export default function ConnectLocation() {
    return (
        <Link to={'/search-location'} className='connect-location-btn'>
            <div className="collect-location">
                <div className="connect-label">Connect to</div>
                <div className="connect-box">
                    <div className="connect-meta">
                        <div className="connect-flag">
                            <img src="/images/flag-vn.png" alt="flag" />
                        </div>

                        <div className="location-name">Viet Nam</div>
                    </div>
                    <div className="connect-icon">
                        <img src="/images/arrow-down.png" alt="arrow-down" />
                    </div>
                </div>
            </div>
        </Link>
    );
}
