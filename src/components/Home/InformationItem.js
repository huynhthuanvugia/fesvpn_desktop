import React from 'react';

export default function InformationItem({ label, value }) {
    return (
        <div className="information-item">
            <div className="information-label">{label}</div>
            <div className="information-value">{value}</div>
        </div>
    );
}
