import React from 'react';

export default function Information({ children, customClass }) {
    return <div className={`information ${customClass}`}>{children}</div>;
}
