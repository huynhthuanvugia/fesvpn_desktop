import React from 'react';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';

export default function MenuItem({
    icon,
    link = '',
    isRunfuncOnclick = false,
    handleOnclick,
}) {
    let resolved = useResolvedPath(link);
    let match = useMatch({ path: resolved.pathname, end: true });

    if (isRunfuncOnclick) {
        return (
            <div className="menu-link" onClick={handleOnclick}>
                <img src={icon} alt="menu-icon" />
            </div>
        );
    }

    return (
        <Link className={`menu-link ${match ? 'active' : ''}`} to={link}>
            <img src={icon} alt="menu-icon" />
        </Link>
    );
}
