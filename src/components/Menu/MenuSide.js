import React from 'react';
import { Link } from 'react-router-dom';
import MenuItem from './MenuItem';

const logOut = () => {
    console.log('Logout');
};

export default function MenuSide() {
    return (
        <div className="app-menu">
            <Link to={'/'} className="logo">
                <img src={'/images/logo.png'} alt="logo" />
            </Link>

            <div className="main-menu">
                <ul className="menu">
                    <li className="menu-item">
                        <MenuItem icon={'/images/icon-home.png'} link={'/'} />
                    </li>
                    <li className="menu-item">
                        <MenuItem
                            icon={'/images/icon-search.png'}
                            link={'/search-location'}
                        />
                    </li>
                    <li className="menu-item">
                        <MenuItem
                            icon={'/images/icon-user.png'}
                            link={'/account'}
                        />
                    </li>
                    <li className="menu-item">
                        <MenuItem
                            icon={'/images/icon-setting.png'}
                            link={'/setting'}
                        />
                    </li>
                    <li className="menu-item">
                        <MenuItem
                            icon={'/images/icon-logout.png'}
                            isRunfuncOnclick={true}
                            handleOnclick={logOut}
                        />
                    </li>
                </ul>
            </div>
        </div>
    );
}
