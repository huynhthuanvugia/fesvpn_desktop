import React from 'react';
import UpgradeOverlay from '../Common/UpgradeOverlay';
import MenuSide from '../Menu/MenuSide';
import { Link } from 'react-router-dom';

export default function LayoutMain({ children }) {
    return (
        <div className="layout layout-main">
            <MenuSide />
            <div className="app-body">
                <div className="app-header">
                    <Link to={'/'} className="logo-mobile">
                        <img src={'/images/logo.png'} alt="logo" />
                    </Link>
                </div>
                <div className="app-content">{children}</div>
            </div>

            <UpgradeOverlay />
        </div>
    );
}
