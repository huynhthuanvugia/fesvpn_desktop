import React from 'react';

export default function LayoutNormal({ children }) {
    return <div className="layout layout-normal">{children}</div>;
}
