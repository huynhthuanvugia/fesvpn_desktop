import React from 'react';

export default function AccountMeta() {
    return (
        <div className="account-meta-box">
            <div className="account-avatar">
                <img src="/images/avatar.png" alt="avatar" />
            </div>

            <div className="account-meta-content">
                <div className="account-name">Username</div>
                <div className="account-email">Festvpn@gmail.com</div>
            </div>
        </div>
    );
}
