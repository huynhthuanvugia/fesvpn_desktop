import React from 'react';

export default function AccountInforList({ children }) {
    return <div className="account-infor-list">{children}</div>;
}
