import React from 'react';

export default function AccountInforItem({ label, value, type, inputName }) {
    if (type === 'dropdown') {
        return (
            <div className="account-infor-item">
                <div className="account-infor-label">{label}</div>
                <div className="account-infor-dropdown">
                    <select name={inputName}>
                        {value.map((item, index) => (
                            <option value={item.value} key={index}>
                                {item.label}
                            </option>
                        ))}
                    </select>
                    <div className="dropdown-icon">
                        <img
                            src="/images/arrow-dropdown.png"
                            alt="arrow-dropdown"
                        />
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="account-infor-item">
            <div className="account-infor-label">{label}</div>
            <div className="account-infor-value">{value}</div>
        </div>
    );
}
