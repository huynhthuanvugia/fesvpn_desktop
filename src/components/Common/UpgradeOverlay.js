import React from 'react';
import { Button } from 'react-bootstrap';

export default function UpgradeOverlay() {
    const [show, setShow] = React.useState(false);

    return (
        <div className={`upgrade-overlay ${show ? '' : 'inactive'}`}>
            <div className="upgrade-content">
                <div className="upgrade-box">
                    <Button className="btn-close-upgrade-overlay">
                        <img src="/images/close-upgrade.png" alt="close" />
                    </Button>
                    <div className="upgrade-label">Get unlimited data</div>
                    <Button className="btn-upgrade-overlay">Upgrade now</Button>
                </div>
            </div>
        </div>
    );
}
