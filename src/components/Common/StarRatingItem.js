import React from 'react';

export default function StarRatingItem({
    setRating,
    ratingValue,
    rating,
    onClickStar,
}) {
    return (
        <div
            className="star-item"
            onClick={() => {
                setRating(ratingValue);
                onClickStar();
            }}
            onMouseEnter={() => setRating(ratingValue)}
        >
            {rating >= ratingValue ? (
                <img src="/images/star-active.png" alt="star-active" />
            ) : (
                <img src="/images/star-inactive.png" alt="star-inactive" />
            )}
        </div>
    );
}
