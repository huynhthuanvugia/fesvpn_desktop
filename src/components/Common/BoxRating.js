import React from 'react';
import StarRatingItem from './StarRatingItem';

export default function BoxRating() {
    const [rating, setRating] = React.useState(1);
    const [show, setShow] = React.useState(false);

    const handleRating = () => {
        console.log('Rating: ' + rating);
    };

    const handleCloseBoxRating = () => {
        setShow(false);
    };

    return (
        <div className={`box-rating ${show ? 'active' : 'inactive'}`}>
            <div className="btn-close-rating" onClick={handleCloseBoxRating}>
                <img src="/images/close-rating.png" alt="close" />
            </div>
            <div className="box-rating-label">Rate your connect speed:</div>
            <div className="rating-star">
                {Array(5)
                    .fill(null)
                    .map((item, index) => (
                        <StarRatingItem
                            key={index}
                            setRating={setRating}
                            rating={rating}
                            ratingValue={index + 1}
                            onClickStar={handleRating}
                        />
                    ))}
            </div>
        </div>
    );
}
