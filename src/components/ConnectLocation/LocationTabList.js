import React from 'react';

export default function LocationTabList({ children }) {
    return <div className="location-tab-list">{children}</div>;
}
