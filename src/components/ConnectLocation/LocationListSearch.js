import React from 'react';
import LocationList from './LocationList';
import SearchBox from './SearchBox';

const locationData = [
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
];

export default function LocationListSearch() {
    return (
        <div className="location-list-search">
            <SearchBox />
            <LocationList
                icon={'/images/icon-location.png'}
                label={'Location'}
                locationList={locationData}
                isShowLikeBtn={true}
            />
        </div>
    );
}
