import React from 'react';
import { Button } from 'react-bootstrap';

export default function SearchBox() {
    return (
        <div className="location-search-box">
            <input
                type="text"
                placeholder="Search by location"
                className="form-control search-location-input"
            />

            <Button className='btn-search-location'>
                <img src="/images/icon-search-location.png" alt="icon-search-location" />
            </Button>
        </div>
    );
}
