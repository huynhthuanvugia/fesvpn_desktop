import React from 'react';
import LocationItem from './LocationItem';

export default function LocationList({
    locationList,
    icon,
    label,
    isShowLikeBtn,
}) {
    return (
        <div className="location-list">
            <div className="location-list-header">
                <div className="location-list-icon">
                    <img src={icon} alt="location-list-icon" />
                </div>
                <div className="location-list-label">{label}</div>
            </div>

            <div className="location-list-content">
                {locationList.map((item, index) => (
                    <LocationItem
                        key={index}
                        flag={item.flag}
                        name={item.name}
                        ping={item.ping}
                        isShowLikeBtn={isShowLikeBtn}
                    />
                ))}
            </div>
        </div>
    );
}
