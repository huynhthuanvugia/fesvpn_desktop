import React from 'react';

export default function LocationItem({
    name,
    flag,
    ping,
    isShowLikeBtn = false,
}) {
    return (
        <div className="location-item">
            <div className="location-meta">
                <div className="location-flag">
                    <img src={flag} alt="flag-location" />
                </div>
                <div className="location-name">{name}</div>
            </div>

            <div className="location-item-action">
                <div className="location-ping">{ping}ms</div>
                {isShowLikeBtn && (
                    <div className="location-like">
                        <img
                            src="/images/icon-header-location-inactive.png"
                            alt="icon-header-location-inactive"
                        />
                    </div>
                )}
            </div>
        </div>
    );
}
