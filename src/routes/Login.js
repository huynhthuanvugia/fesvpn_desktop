import React from 'react';
import FormLogin from '../components/Forms/FormLogin';

export default function Login() {
    return (
        <div className="login-content">
            <div className="login-form">
                <FormLogin />
            </div>
            <div className="login-decor">
                <div className="decor-login-img">
                    <img src="/images/login-decor.png" alt="login-decor" />
                </div>
            </div>
        </div>
    );
}
