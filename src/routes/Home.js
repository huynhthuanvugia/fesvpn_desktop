import React from 'react';
import BoxRating from '../components/Common/BoxRating';
import ButtonConnect from '../components/Home/ButtonConnect';
import ConnectLocation from '../components/Home/ConnectLocation';
import Information from '../components/Home/Information';
import InformationBar from '../components/Home/InformationBar';
import InformationItem from '../components/Home/InformationItem';
import LocationBackground from '../components/Home/LocationBackground';
import Remain from '../components/Home/Remain';
import TimeRemain from '../components/Home/TimeRemain';

export default function Home() {
    return (
        <div className="home-content">
            <div className="home-content-inner">
                <ConnectLocation />

                <Information>
                    <InformationItem
                        label={'Your local IP'}
                        value={'85.108.104.235'}
                    />
                    <InformationBar />
                    <InformationItem
                        label={'VPN IP'}
                        value={'86.107.104.235'}
                    />
                </Information>

                <ButtonConnect />

                <TimeRemain />

                <Information customClass={'information-data-transfer'}>
                    <InformationItem label={'Download'} value={'2.5 MB'} />
                    <InformationBar />
                    <InformationItem label={'Upload'} value={'2.5 MB'} />
                </Information>

                <Remain />

                <BoxRating />

                <LocationBackground />
            </div>
        </div>
    );
}
