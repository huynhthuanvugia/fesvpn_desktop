import React from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import TabConnectionFeature from '../components/Settings/SettingTabs/TabConnectionFeature';
import TabGeneral from '../components/Settings/SettingTabs/TabGeneral';
import TabProtocol from '../components/Settings/SettingTabs/TabProtocol';

export default function Setting() {
    return (
        <div className="setting-content">
            <div className="setting-tab-wrapper">
                <Tabs defaultActiveKey="general" className="setting-tabs">
                    <Tab eventKey="general" title="General">
                        <TabGeneral />
                    </Tab>
                    <Tab eventKey="protocol" title="Protocol">
                        <TabProtocol />
                    </Tab>
                    <Tab eventKey="connect-feature" title="Connection feature">
                        <TabConnectionFeature />
                    </Tab>
                </Tabs>
            </div>
        </div>
    );
}
