import React from 'react';
import LocationList from '../components/ConnectLocation/LocationList';
import LocationListSearch from '../components/ConnectLocation/LocationListSearch';
import LocationTabList from '../components/ConnectLocation/LocationTabList';

const locationData = [
    {
        flag: '/images/flag-vn.png',
        name: 'United Kingdom',
        ping: 75,
    },
];

export default function SearchLocaltion() {
    return (
        <div className="search-location-content">
            <div className="location-panels">
                <LocationTabList>
                    <LocationList
                        icon={'/images/icon-location.png'}
                        label={'Recent Location'}
                        locationList={locationData}
                    />

                    <LocationList
                        icon={'/images/icon-star.png'}
                        label={'Recommended Server'}
                        locationList={locationData}
                    />

                    <LocationList
                        icon={'/images/icon-heart.png'}
                        label={'Favorites Server'}
                        locationList={locationData}
                    />
                </LocationTabList>

                <LocationTabList>
                    <LocationListSearch />
                </LocationTabList>
            </div>
        </div>
    );
}
