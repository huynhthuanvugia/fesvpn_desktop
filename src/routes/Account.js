import React from 'react';
import AccountInforItem from '../components/Account/AccountInforItem';
import AccountInforList from '../components/Account/AccountInforList';
import AccountMeta from '../components/Account/AccountMeta';

const dataDropdown = [
    {
        value: 'en',
        label: 'English',
    },
    {
        value: 'vi',
        label: 'Viet Nam',
    },
    {
        value: 'cn',
        label: 'China',
    },
    {
        value: 'ru',
        label: 'Russian',
    },
];

export default function Account() {
    return (
        <div className="account-content">
            <AccountMeta />

            <AccountInforList>
                <AccountInforItem label={'Plan:'} value={'Trial'} />
                <AccountInforItem label={'Expiries on:'} value={'Trial'} />
                <AccountInforItem
                    label={'Active connections:'}
                    value={'Trial'}
                />
                <AccountInforItem
                    label={'Languages:'}
                    value={dataDropdown}
                    type={'dropdown'}
                    inputName={'language'}
                />
            </AccountInforList>
        </div>
    );
}
