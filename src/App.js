import { Routes, Route } from 'react-router-dom';

import './App.css';
import './assets/fonts/stylesheet.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import LayoutMain from './components/Layouts/LayoutMain';
import Home from './routes/Home';
import SearchLocaltion from './routes/SearchLocaltion';
import Account from './routes/Account';
import Setting from './routes/Setting';
import Login from './routes/Login';
import LayoutNormal from './components/Layouts/LayoutNormal';

function App() {
    return (
        <div className="App">
            {true ? (
                <LayoutMain>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route
                            path="search-location"
                            element={<SearchLocaltion />}
                        />
                        <Route path="account" element={<Account />} />
                        <Route path="setting" element={<Setting />} />
                    </Routes>
                </LayoutMain>
            ) : (
                <LayoutNormal>
                    <Routes>
                        <Route path="/" element={<Login />} />
                    </Routes>
                </LayoutNormal>
            )}
        </div>
    );
}

export default App;
